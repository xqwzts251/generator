package generator

import java.nio.file.{Files, Paths}

import generator.generators._
import generator.tables.Tables

object Main {
  def main(args: Array[String]) {
    try {
      generateData()
      val script = sqlScriptFromTables()
      saveToFile(script, filePath = "gen.sql")
    } catch {
      case e: Exception => println(e.getClass, e.getMessage)
    }
  }

  private def generateData(): Unit = {
    val participantGenerator = new ParticipantGenerator("names.txt")
    val locationGenerator = new LocationGenerator("locations.txt")
    val clientGenerator = new ClientGenerator("names.txt", "companyNames.txt", "addresses.txt")
    val conferenceGenerator = new ConferenceGenerator("conferenceNames.txt", "conferenceDesc.txt", "workshopDesc.txt")
    val bookingGenerator = new BookingGenerator

    participantGenerator.generate(15000, 16000)
    locationGenerator.generate()
    clientGenerator.generate(2500, 3000)
    conferenceGenerator.generate(70, 80)
    bookingGenerator.generate()
  }

  private def saveToFile(text: String, filePath: String): Unit = {
    val path = Paths.get(filePath)
    val bytes = text.getBytes
    Files.write(path, bytes)
  }

  private def sqlScriptFromTables() = {
    val tables = List(
      Tables.client,
      Tables.country,
      Tables.city,
      Tables.conference,
      Tables.conferenceDay,
      Tables.price,
      Tables.conferenceBooking,
      Tables.conferenceDayBooking,
      Tables.workshop,
      Tables.workshopBooking,
      Tables.participant,
      Tables.conferenceDayRegistration,
      Tables.workshopRegistration,
      Tables.payment
    )

    tables.map(_.reverse).flatMap(_.map(_.sqlCode)).mkString("\n")
  }
}