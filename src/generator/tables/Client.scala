package generator.tables

import generator._

object Client {
  def company(name: SqlString, address: SqlType, phone: SqlInt) = {
    val client = new Client(SqlNull, SqlNull, name, address, true, phone)
    Tables.client ::= client
    client
  }

  def person(name: SqlString, lastName: SqlString, address: SqlType, phone: SqlInt) = {
    val client = new Client(name, lastName, SqlNull, address, false, phone)
    Tables.client ::= client
    client
  }
}

class Client private(
    val name: SqlType,
    val lastName: SqlType,
    val companyName: SqlType,
    val address: SqlType,
    val isCompany: SqlBit,
    val phone: SqlInt)
  extends SqlTable("Klient") {
  val id = Tables.client.nextId

  protected override def attributeList = {
    List(
      ("Imie", name),
      ("Nazwisko", lastName),
      ("Nazwa_firmy", companyName),
      ("Adres", address),
      ("Czy_firma", isCompany),
      ("Telefon", phone))
  }
}