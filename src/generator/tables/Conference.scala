package generator.tables

import generator._

object Conference {
  def apply(daysNumber: SqlInt, firstDayDate: SqlDate, studentDiscount: SqlFloat, name: SqlString, cityId: SqlInt, description: SqlString) = {
    val conference = new Conference(daysNumber, firstDayDate, studentDiscount, name, cityId, description, false)
    Tables.conference ::= conference
    conference
  }
}

class Conference private(val daysNumber: SqlInt, val firstDayDate: SqlDate, val studentDiscount: SqlFloat, val name: SqlString, val cityId: SqlInt, val description: SqlString, val isCancelled: SqlBit)
  extends SqlTable("Konferencja") {
  val id = Tables.conference.nextId

  override protected def attributeList: List[(String, SqlType)] = {
    List(("Ilosc_dni", daysNumber), ("Data_pierwszego_dnia", firstDayDate), ("Znizka_studencka", studentDiscount), ("Nazwa", name), ("Id_miasto", cityId), ("Opis", description), ("Czy_anulowana", isCancelled))
  }
}
