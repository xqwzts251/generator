package generator.tables

import generator._

object WorkshopRegistration {
  def apply(workshopBookingId: SqlInt, conferenceDayRegistrationId: SqlInt) = {
    val registration = new WorkshopRegistration(workshopBookingId, conferenceDayRegistrationId)
    Tables.workshopRegistration ::= registration
    registration
  }
}

class WorkshopRegistration private(val workshopBookingId: SqlInt, val conferenceDayRegistrationId: SqlInt)
  extends SqlTable("Rejestracja_warsztatu") {
  val id = Tables.workshopRegistration.nextId

  override protected def attributeList: List[(String, SqlType)] = {
    List(
      ("Id_rezerwacja_warsztatu", workshopBookingId),
      ("Id_rejestracja_dnia_konferencji", conferenceDayRegistrationId))
  }
}
