package generator.tables

import generator._

object Payment {
  def apply(paymentDate: SqlDate, amount: SqlMoney, conferenceBookingId: SqlInt) = {
    val payment = new Payment(paymentDate, amount, conferenceBookingId)
    Tables.payment ::= payment
    payment
  }
}

class Payment private(
    val paymentDate: SqlDate,
    val amount: SqlMoney,
    val conferenceBookingId: SqlInt)
  extends SqlTable("Wplata") {
  val id = Tables.payment.nextId

  override protected def attributeList: List[(String, SqlType)] = {
    List(
      ("Data_wplaty", paymentDate),
      ("Kwota", amount),
      ("Id_rezerwacja_konferencji", conferenceBookingId))
  }
}