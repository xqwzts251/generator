package generator.tables

import generator._

object WorkshopBooking {
  def apply(conferenceDayBookingId: SqlInt, workshopId: SqlInt, seatsNumber: SqlInt) = {
    val booking = new WorkshopBooking(conferenceDayBookingId, workshopId, seatsNumber)
    Tables.workshopBooking ::= booking
    booking
  }
}

class WorkshopBooking(val conferenceDayBookingId: SqlInt, val workshopId: SqlInt, val seatsNumber: SqlInt)
  extends SqlTable("Rezerwacja_warsztatu") {
  val id = Tables.workshopBooking.nextId

  override protected def attributeList: List[(String, SqlType)] = {
    List(
      ("Id_rezerwacja_dnia_konferencji", conferenceDayBookingId),
      ("Id_warsztat", workshopId), ("Ilosc_miejsc", seatsNumber))
  }
}