package generator

package object tables {
  implicit def list2List[T](list: List[T]): ListWithNextId[T] = new ListWithNextId[T](list)

  class ListWithNextId[T](val list: List[T]) {
    def nextId = list.size + 1
  }
}
