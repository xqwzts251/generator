package generator.tables

import generator.SqlType

abstract class SqlTable(val tableName: String) {
  def sqlCode: String = {
    val (attributeNames, attributeValues) = attributeList.unzip
    val namesString = attributeNames.mkString("(", ", ", ")")
    val valuesString = attributeValues.map(_.sqlString).mkString("(", ", ", ")")
    s"""INSERT INTO $tableName $namesString
		   |VALUES $valuesString
		   |""".stripMargin
  }

  protected def attributeList: List[(String, SqlType)]
}