package generator.tables

import generator._

object City {
  def apply(countryId: SqlInt, name: SqlString) = {
    val city = new City(countryId, name)
    Tables.city ::= city
    city
  }
}

class City private(val countryId: SqlInt, val name: SqlString) extends SqlTable("Miasto") {
  val id = Tables.city.nextId

  override protected def attributeList = List(("Id_kraj", countryId), ("Nazwa", name))
}
