package generator.tables

import generator._

object ConferenceBooking {
  def apply(clientId: SqlInt) = {
    val booking = new ConferenceBooking(clientId, false)
    Tables.conferenceBooking ::= booking
    booking
  }
}

class ConferenceBooking(val clientId: SqlInt, val isCancelled: SqlBit) extends SqlTable("Rezerwacja_konferencji") {
  val id = Tables.conferenceBooking.nextId

  override protected def attributeList: List[(String, SqlType)] = {
    List(("Id_klient", clientId), ("Czy_anulowana", isCancelled))
  }
}