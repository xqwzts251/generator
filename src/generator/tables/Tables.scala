package generator.tables

object Tables {
  var client: List[Client] = List()
  var country: List[Country] = List()
  var city: List[City] = List()
  var conference: List[Conference] = List()
  var conferenceDay: List[ConferenceDay] = List()
  var price: List[Price] = List()
  var conferenceBooking: List[ConferenceBooking] = List()
  var conferenceDayBooking: List[ConferenceDayBooking] = List()
  var workshop: List[Workshop] = List()
  var workshopBooking: List[WorkshopBooking] = List()
  var participant: List[Participant] = List()
  var conferenceDayRegistration: List[ConferenceDayRegistration] = List()
  var workshopRegistration: List[WorkshopRegistration] = List()
  var payment: List[Payment] = List()
}