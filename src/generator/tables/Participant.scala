package generator.tables

import generator._

object Participant {
  def apply(firstName: SqlString, lastName: SqlString) = {
    val participant = new Participant(firstName, lastName)
    Tables.participant ::= participant
    participant
  }
}

class Participant private(val firstName: SqlString, val lastName: SqlString) extends SqlTable("Uczestnik") {
  val id = Tables.participant.nextId

  override protected def attributeList = List(("Imie", firstName), ("Nazwisko", lastName))
}
