package generator.tables

import generator._

object ConferenceDay {
  def apply(conferenceId: SqlInt, dayNumber: SqlInt, seatsNumber: SqlInt) = {
    val day = new ConferenceDay(conferenceId, dayNumber, seatsNumber)
    Tables.conferenceDay ::= day
    day
  }
}

class ConferenceDay private(
    val conferenceId: SqlInt,
    val dayNumber: SqlInt,
    val seatsNumber: SqlInt)
  extends SqlTable("Dzien_konferencji") {
  val id = Tables.conferenceDay.nextId

  override protected def attributeList: List[(String, SqlType)] = {
    List(
      ("Id_konferencja", conferenceId),
      ("Nr_dnia", dayNumber),
      ("Ilosc_miejsc", seatsNumber))
  }
}