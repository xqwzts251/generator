package generator.tables

import generator._

object Country {
  def apply(name: SqlString) = {
    val country = new Country(name)
    Tables.country ::= country
    country
  }
}

class Country private(val name: SqlString) extends SqlTable("Kraj") {
  val id = Tables.country.nextId

  override protected def attributeList = List(("Nazwa", name))
}
