package generator.tables

import generator._

object ConferenceDayRegistration {
  def notStudent(participantId: SqlInt, conferenceDayBookingId: SqlInt) = {
    val registration = new ConferenceDayRegistration(participantId, conferenceDayBookingId, SqlNull)
    Tables.conferenceDayRegistration ::= registration
    registration
  }

  def student(participantId: SqlInt, conferenceDayBookingId: SqlInt, studentCardNumber: SqlType) = {
    val registration = new ConferenceDayRegistration(participantId, conferenceDayBookingId, studentCardNumber)
    Tables.conferenceDayRegistration ::= registration
    registration
  }
}

class ConferenceDayRegistration private(
    val participantId: SqlInt,
    val conferenceDayBookingId: SqlInt,
    val studentCardNumber: SqlType)
  extends SqlTable("Rejestracja_dnia_konferencji") {
  val id = Tables.conferenceDayRegistration.nextId

  override protected def attributeList: List[(String, SqlType)] = {
    List(
      ("Id_uczestnik", participantId),
      ("Id_rezerwacja_dnia_konferencji", conferenceDayBookingId),
      ("Nr_legitymacji", studentCardNumber))
  }

  def isStudent = studentCardNumber != SqlNull
}
