package generator.tables

import generator._

object Workshop {
  def apply(startTime: SqlTime,
      endTime: SqlTime,
      description: SqlString,
      seatsNumber: SqlInt,
      price: SqlMoney,
      conferenceDayId: SqlInt) = {
    val workshop = new Workshop(startTime, endTime, description, seatsNumber, price, conferenceDayId)
    Tables.workshop ::= workshop
    workshop
  }
}

class Workshop private(
    val startTime: SqlTime,
    val endTime: SqlTime,
    val description: SqlString,
    val seatsNumber: SqlInt,
    val price: SqlMoney,
    val conferenceDayId: SqlInt)
  extends SqlTable("Warsztat") {
  val id = Tables.workshop.nextId

  private var seatsLeft = seatsNumber

  def book(seats: Int): Unit = seatsLeft -= seats

  def freeSeats = seatsLeft

  override protected def attributeList: List[(String, SqlType)] = {
    List(
      ("Godzina_rozpoczecia", startTime),
      ("Godzina_zakonczenia", endTime),
      ("Opis", description),
      ("Ilosc_miejsc", seatsNumber),
      ("Cena", price),
      ("Id_dzien_konferencji", conferenceDayId))
  }
}