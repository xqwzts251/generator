package generator.tables

import generator._

object Price {
  def apply(conferenceId: SqlInt, amount: SqlMoney, dateTo: SqlDate) = {
    val price = new Price(conferenceId, amount, dateTo)
    Tables.price ::= price
    price
  }
}

class Price private(val conferenceId: SqlInt, val amount: SqlMoney, val dateTo: SqlDate) extends SqlTable("Cena") {
  val id = Tables.price.nextId

  override protected def attributeList: List[(String, SqlType)] = {
    List(("Id_konferencja", conferenceId), ("Kwota", amount), ("Data_do", dateTo))
  }
}
