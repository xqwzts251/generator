package generator.tables

import generator._

object ConferenceDayBooking {
  def apply(seatsNumber: SqlInt, conferenceDayId: SqlInt, conferenceBookingId: SqlInt, bookingDate: SqlDate) = {
    val booking = new ConferenceDayBooking(seatsNumber, conferenceDayId, conferenceBookingId, bookingDate)
    Tables.conferenceDayBooking ::= booking
    booking
  }
}

class ConferenceDayBooking private(val seatsNumber: SqlInt, val conferenceDayId: SqlInt, val conferenceBookingId: SqlInt, val bookingDate: SqlDate)
  extends SqlTable("Rezerwacja_dnia_konferencji") {
  val id = Tables.conferenceDayBooking.nextId

  override protected def attributeList: List[(String, SqlType)] = {
    List(("Ilosc_miejsc", seatsNumber), ("Id_dzien_konferencji", conferenceDayId), ("Id_rezerwacja_konferencji", conferenceBookingId), ("Data_rezerwacji", bookingDate))
  }
}
