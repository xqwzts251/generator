package generator.generators

import generator._
import generator.tables.Participant

class ParticipantGenerator(val namesFilePath: String) extends Generator {
  private val names = parseNames(namesFilePath).shuffle

  def generate(from: Int, to: Int): Unit = {
    val indices = randomRange(from, to)

    def generateOne(index: Int) = {
      val (firstName, lastName) = names(index % names.size)
      Participant(firstName, lastName)
    }

    indices.foreach(generateOne)
    println("Completed generating participants")
  }
}
