package generator.generators

import generator.tables.{City, Country}

class LocationGenerator(val locationsFilePath: String) extends Generator {
  private val locations = lines(locationsFilePath).map(_.split(":")).map({ case Array(a, b) => (a, b.split(",").toList.map(_.trim))})

  def generate(): Unit = {
    type Location = (String, List[String])

    def makeLocation(location: Location) = {
      val (countryName, cities) = location

      val country = Country(countryName)

      def makeCity(cityName: String) = City(country.id, cityName)

      cities.foreach(makeCity)
    }

    locations.foreach(makeLocation)
    println("Completed generating locations")
  }
}