package generator

import scala.io.Source
import scala.util.Random

package object generators {
  val random = new Random(System.currentTimeMillis)

  def parseNames(namesFilePath: String) = lines(namesFilePath).map(_.split(",")).map({ case Array(a, b) => (a, b)})

  def lines(filePath: String) = Source.fromFile(filePath)("UTF-8").getLines().map(_.trim).filterNot(_.isEmpty).toList

  implicit def list2List[T](list: List[T]): EnhancedList[T] = new EnhancedList[T](list)

  class EnhancedList[T](val list: List[T]) {
    def shuffle = Random.shuffle(list)

    def randomElement = list(random.nextInt(list.size))

    def makeChunks(chunksNumber: Int): List[List[T]] = {
      require(list.size >= chunksNumber)
      val chunks = Array.ofDim[List[T]](chunksNumber)
      def initChunks(index: Int, list: List[T]): List[T] = {
        if (index < chunksNumber) {
          val head :: tail = list
          chunks(index) = List(head)
          initChunks(index + 1, tail)
        }
        else list
      }
      val restOfList = initChunks(0, list)
      def addToRandomChunk(element: T) = chunks(random.nextInt(chunksNumber)) ::= element
      restOfList.foreach(addToRandomChunk)
      chunks.toList
    }
  }
}
