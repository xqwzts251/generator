package generator.generators

import com.github.nscala_time.time.Imports._
import generator._
import generator.tables.{Conference, Tables}

object ConferenceGenerator {
  private val ConferencesPerYear = 24
}

class ConferenceGenerator(val conferenceNamesFilePath: String, val conferenceDescFilePath: String, val workshopDescFilePath: String) extends Generator {
  private val conferenceNames = lines(conferenceNamesFilePath)
  private val conferenceDescriptions = lines(conferenceDescFilePath)
  private val workshopDescriptions = lines(workshopDescFilePath)
  private val priceGenerator = new PriceGenerator
  private val conferenceDayGenerator = new ConferenceDayGenerator(workshopDescriptions)

  def generate(from: Int, to: Int): Unit = {
    val numberOfConferences = randomIntInRange(from, to)
    val daysToCover = 365 * numberOfConferences / ConferenceGenerator.ConferencesPerYear
    val daysInThePast = randomIntInRange(75, 85) * daysToCover / 100
    val firstConferenceDate = DateTime.now - daysInThePast.days

    def makeConference(number: Int, date: DateTime): Unit = {
      if (number < numberOfConferences) {
        val duration = randomIntInRange(2, 3)
        val discount = randomIntInRange(0, 40, step = 5) / 100.0
        val name = conferenceNames.randomElement
        val description = conferenceDescriptions.randomElement
        val cityId = Tables.city.randomElement.id

        val conference = Conference(duration, date, discount, name, cityId, description)
        priceGenerator.generate(conference)
        conferenceDayGenerator.generate(conference)

        makeConference(number + 1, date + daysBetweenConferences)
      }
    }

    makeConference(0, firstConferenceDate)
    println("Completed generating conferences")
  }

  private def daysBetweenConferences = randomIntInRange(1, 29).days
}