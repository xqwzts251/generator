package generator.generators

import generator.tables.{Conference, ConferenceDay}

class ConferenceDayGenerator(val descriptions: List[String]) extends Generator {
  val workshopGenerator = new WorkshopGenerator(descriptions)

  def generate(conference: Conference) = {
    def makeDay(dayNumber: Int) = {
      val day = ConferenceDay(conference.id, dayNumber, randomSeatsNumber)
      workshopGenerator.generate(day)
    }

    val dayNumbers = 1 to conference.daysNumber
    dayNumbers.foreach(makeDay)
  }

  private def randomSeatsNumber = randomIntInRange(100, 300, step = 10)
}
