package generator.generators

import com.github.nscala_time.time.Imports._
import generator.tables.{ConferenceBooking, Payment}

object PaymentGenerator {
  private val DaysToPay = 7
}

class PaymentGenerator extends Generator {
  def generate(costOfBooking: Double, booking: ConferenceBooking, bookingDate: DateTime) = {
    val bookingId = booking.id
    val paymentsNumber = randomIntInRange(1, 4)
    val amountsPaid = splitAmount(costOfBooking, paymentsNumber)
    val dayNumbers = randomDayNumbers(paymentsNumber)
    val daysWithAmounts = dayNumbers zip amountsPaid
    val newDaysWithAmounts = randomDrop(daysWithAmounts)

    def makePayment(data: (Int, Double)): Unit = {
      val (dayNo, amount) = data
      Payment(bookingDate + dayNo.days, amount, bookingId)
    }

    newDaysWithAmounts.foreach(makePayment)
  }

  private def randomDrop[T](list: List[T]) = {
    val shouldDrop = probabilityOf(0.02)
    if (shouldDrop) list diff List(list.randomElement)
    else list
  }

  private def splitAmount(amount: Double, parts: Int) = {
    val initialAmount: Double = (amount / 100 / 5).round * 100
    val initialSplit = List.fill(parts)(initialAmount)
    val missingAmount = amount - initialSplit.sum
    val newValue = initialSplit(0) + missingAmount
    initialSplit.updated(0, newValue).shuffle
  }

  private def randomDayNumbers(numberOfDays: Int) = {
    val days = List.range(0, PaymentGenerator.DaysToPay + 1)
    days.shuffle.take(numberOfDays).sortWith(_ < _)
  }
}
