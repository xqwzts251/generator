package generator.generators

trait Generator {
  protected def randomDoubleInRange(from: Double, to: Double) = random.nextDouble() * (to - from) + from

  protected def randomIntInRange(from: Int, to: Int, step: Int = 1): Int = {
    val number = random.nextInt(to - from + 1) + from
    if (step <= 1) number
    else {
      val intWithStep = ((number / step.toDouble).round * step).toInt
      if (intWithStep > to) intWithStep - step
      else if (intWithStep < from) intWithStep + step
      else intWithStep
    }
  }

  protected def randomRange(from: Int, to: Int) = 0 until randomIntInRange(from, to)

  protected def probabilityOf(value: Double) = {
    val probability = (value * 100).toInt
    val randomInt = randomIntInRange(0, 99)

    randomInt < probability
  }

  protected def randomRangeWithProbability(from: Int, to: Int, probability: Double) = {
    val shouldDraw = probabilityOf(0.05)
    if (shouldDraw) randomIntInRange(from, to)
    else 0
  }
}
