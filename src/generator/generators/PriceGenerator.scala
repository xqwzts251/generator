package generator.generators

import com.github.nscala_time.time.Imports._
import generator.tables.{Conference, Price}

class PriceGenerator extends Generator {
  def generate(conference: Conference) = {
    val pricesNumber = randomIntInRange(1, 3)
    val finalPrice = randomIntInRange(350, 650, step = 50)
    val finalDate: DateTime = conference.firstDayDate

    def go(priceNumber: Int, price: Double, date: DateTime): Unit = {
      if (priceNumber < pricesNumber) {
        Price(conference.id, price, date)
        go(priceNumber + 1, price - randomPriceDifference, date - randomDateDifference)
      }
    }

    go(0, finalPrice, finalDate)
  }

  private def randomPriceDifference = randomIntInRange(50, 150, step = 50)
  private def randomDateDifference = randomIntInRange(1, 3).weeks
}
