package generator.generators

import com.github.nscala_time.time.Imports._
import generator._
import generator.tables.{ConferenceDay, Workshop}

class WorkshopGenerator(val descriptions: List[String]) extends Generator {
  def generate(conferenceDay: ConferenceDay) = {
    val workshopsNumber = randomIntInRange(3, 5)
    val firstStartTime = randomStartTime

    def go(workshopNumber: Int, startTime: DateTime): Unit = {
      if (workshopNumber < workshopsNumber) {
        val endTime = startTime + randomDuration
        val description = descriptions.randomElement
        val seatsNumber = randomIntInRange(10, conferenceDay.seatsNumber, step = 10)
        val price = randomIntInRange(0, 600, step = 50).toDouble

        Workshop(startTime, endTime, description, seatsNumber, price, conferenceDay.id)

        go(workshopNumber + 1, endTime + randomBreak)
      }
    }

    go(0, firstStartTime)
  }

  private def randomDuration = randomIntInRange(30, 90, step = 30).minutes
  private def randomBreak = randomIntInRange(0, 60, step = 30).minutes
  private def randomStartTime = {
    val zeroTime = DateTime.parse("00:00", DateTimeFormat.forPattern("HH:ss"))
    val timeOfDay = randomIntInRange(7, 12).hours + (randomIntInRange(0, 1) * 30).minutes
    zeroTime + timeOfDay
  }
}
