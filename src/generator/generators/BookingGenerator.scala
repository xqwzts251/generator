package generator.generators

import com.github.nscala_time.time.Imports._
import generator._
import generator.tables._

class BookingGenerator extends Generator {
  val paymentGenerator = new PaymentGenerator

  def generate(): Unit = {
    Tables.conference.foreach(makeBookingsForConference)
    println("Completed generating bookings")
  }

  def makeBookingsForConference(conference: Conference) {
    val bookingsNumber = randomIntInRange(3, 60)
    val clients = Tables.client.shuffle.take(bookingsNumber)
    val days = Tables.conferenceDay.filter(_.conferenceId.value == conference.id)
    val participantsNumber = days.map(_.seatsNumber.value).max
    val allParticipants = Tables.participant.shuffle.take(participantsNumber)
    val participantGroups = allParticipants.makeChunks(bookingsNumber)
    val studentMultiplier = 1 - conference.studentDiscount.value

    def makeBooking(bookingIndex: Int) = {
      val clientId = clients(bookingIndex).id
      val participants = participantGroups(bookingIndex)
      val bookingDate = conferenceBookingDate(conference)
      val conferenceBooking = ConferenceBooking(clientId)

      var costOfBooking: Double = 0
      val priceForDayRegistration = conferencePrice(bookingDate, conference)

      def makeDayBooking(conferenceDay: ConferenceDay): Unit = {
        val availableSeats = conferenceDay.seatsNumber
        val bookedSeats = math.min(participants.size + extraBookedSeats, availableSeats)
        val conferenceDayId = conferenceDay.id
        val conferenceBookingId = conferenceBooking.id
        val participantsForDay = participants.shuffle.drop(randomIntInRange(0, participants.size / 10)).take(availableSeats)
        val dayBooking = ConferenceDayBooking(bookedSeats, conferenceDayId, conferenceBookingId, bookingDate)
        val workshops = Tables.workshop.filter(_.conferenceDayId.value == conferenceDayId)

        def makeWorkshopBooking(workshop: Workshop): Option[WorkshopBooking] = {
          val seatsLimit = math.min(workshop.freeSeats, bookedSeats)
          if (seatsLimit > 0) {
            val seats = randomIntInRange(1, seatsLimit)
            workshop.book(seats)
            val booking = WorkshopBooking(dayBooking.id, workshop.id, seats)
            Some(booking)
          } else None
        }
        val workshopBookings = workshops.flatMap(makeWorkshopBooking)

        def registerParticipant(participant: Participant): ConferenceDayRegistration = {
          val isStudent = probabilityOf(0.2)
          if (isStudent) {
            val studentCardNumber = randomIntInRange(100000, 999999)
            costOfBooking += priceForDayRegistration * studentMultiplier
            ConferenceDayRegistration.student(participant.id, dayBooking.id, studentCardNumber)
          } else {
            costOfBooking += priceForDayRegistration
            ConferenceDayRegistration.notStudent(participant.id, dayBooking.id)
          }
        }
        val dayRegistrations = participantsForDay.map(registerParticipant)

        var tempRegistrations = dayRegistrations.shuffle
        def registerForWorkshopBooking(workshopBooking: WorkshopBooking) = {
          if (tempRegistrations.size > 0) {
            val priceForWorkshop = workshopPrice(workshopBooking)
            val registrationsNumber = workshopBooking.seatsNumber - unregisteredWorkshopSeats

            val registrations = tempRegistrations.take(registrationsNumber)
            tempRegistrations = tempRegistrations.drop(registrationsNumber)

            def registerForWorkshop(registration: ConferenceDayRegistration) = {
              if (registration.isStudent) costOfBooking += priceForWorkshop * studentMultiplier
              else costOfBooking += priceForWorkshop
              WorkshopRegistration(workshopBooking.id, registration.id)
            }

            registrations.foreach(registerForWorkshop)
          }
        }
        workshopBookings.foreach(registerForWorkshopBooking)
      }
      days.foreach(makeDayBooking)
      paymentGenerator.generate(costOfBooking, conferenceBooking, bookingDate)
    }

    val bookingIndices = 0 until bookingsNumber
    bookingIndices.foreach(makeBooking)
  }

  private def workshopPrice(workshopBooking: WorkshopBooking): Double =
    Tables.workshop.filter(_.id == workshopBooking.workshopId.value)(0).price.value

  private def conferencePrice(bookingDate: DateTime, conference: Conference): Double = {
    Tables.price
      .filter(_.conferenceId.value == conference.id)
      .filter(_.dateTo.value > bookingDate)
      .minBy(_.dateTo.value)
      .amount.value
  }

  private def conferenceBookingDate(conference: Conference) = {
    val firstDayDate: DateTime = conference.firstDayDate
    val latestPossibleBookingDate = firstDayDate - 14.days
    val daysBefore = randomIntInRange(0, 30).days
    latestPossibleBookingDate - daysBefore
  }

  private def unregisteredWorkshopSeats = randomRangeWithProbability(1, 2, 0.05)

  private def extraBookedSeats = randomRangeWithProbability(1, 15, 0.1)
}
