package generator.generators

import generator._
import generator.tables.Client

import scala.annotation.tailrec

class ClientGenerator(val namesFilePath: String, val companyNamesFilePath: String, val addressesFilePath: String)
  extends Generator {
  private val names = parseNames(namesFilePath).shuffle
  private val companyNames = lines(companyNamesFilePath).shuffle
  private val addresses = lines(addressesFilePath).shuffle

  def generate(from: Int, to: Int): Unit = {
    val numberOfClients = randomIntInRange(from, to)

    @tailrec
    def go(clientIndex: Int, companyIndex: Int, personIndex: Int): Unit = {
      def makeCompany() = {
        val companyName = companyNames(companyIndex % companyNames.size)
        val address = addresses(clientIndex)
        val phone = randomPhone

        Client.company(companyName, address, phone)
      }

      def makePerson() = {
        val (firstName, lastName) = names(personIndex % names.size)
        val address = addresses(clientIndex)
        val phone = randomPhone

        Client.person(firstName, lastName, address, phone)
      }

      if (clientIndex < numberOfClients) {
        val shouldMakeCompany = probabilityOf(0.65)
        if (shouldMakeCompany) {
          makeCompany()
          go(clientIndex + 1, companyIndex + 1, personIndex)
        } else {
          makePerson()
          go(clientIndex + 1, companyIndex, personIndex + 1)
        }
      }
    }

    go(0, 0, 0)
    println("Completed generating clients")
  }

  private def randomPhone: Int = randomIntInRange(100000000, 999999999)
}
