import com.github.nscala_time.time.Imports
import com.github.nscala_time.time.Imports._

package object generator {
  val SqlNull = new SqlNull

  implicit def int2SqlInt(int: Int): SqlInt = new SqlInt(int)

  implicit def date2SqlDate(dateTime: DateTime): SqlDate = new SqlDate(dateTime)

  implicit def string2SqlString(string: String): SqlString = new SqlString(string)

  implicit def boolean2SqlBit(boolean: Boolean): SqlBit = new SqlBit(boolean)

  implicit def double2SqlMoney(value: Double): SqlMoney = new SqlMoney(value)

  implicit def double2SqlFloat(value: Double): SqlFloat = new SqlFloat(value)

  implicit def time2SqlTime(time: DateTime): SqlTime = new SqlTime(time)

  implicit def sqlDate2Date(date: SqlDate): Imports.DateTime = date.value

  implicit def sqlInt2Int(sqlInt: SqlInt): Int = sqlInt.value

  trait SqlType {
    def sqlString: String
  }

  class SqlNull extends SqlType {
    override def sqlString: String = "NULL"
  }

  class SqlInt(val value: Int) extends SqlType {
    override def sqlString: String = value.toString
  }

  class SqlDate(val value: DateTime) extends SqlType {
    override def sqlString: String = {
      val formatted = DateTimeFormat.forPattern("yyyy-MM-dd").print(value)
      s"'$formatted'"
    }
  }

  class SqlString(val value: String) extends SqlType {
    override def sqlString: String = {
      val fixedString = value.replaceAll("'", "''")
      s"'$fixedString'"
    }
  }

  class SqlBit(val value: Boolean) extends SqlType {
    override def sqlString: String = if (value) "1" else "0"
  }

  class SqlMoney(val value: Double) extends SqlType {
    override def sqlString: String = "%.2f".formatLocal(java.util.Locale.US, value)
  }

  class SqlFloat(val value: Double) extends SqlType {
    override def sqlString: String = "%.2f".formatLocal(java.util.Locale.US, value)
  }

  class SqlTime(val value: DateTime) extends SqlType {
    override def sqlString: String = {
      val formatted = DateTimeFormat.forPattern("HH:mm").print(value)
      s"'$formatted'"
    }
  }
}